本项目完全由我本人开发完成，主要功能是为D2SE v2.2.0增加1.13d版本的支持，使用方法如下：  
1、将install下的1.13d目录复制到D:\Diablo II D2SE\D2SE\CORES下面（具体目录要看你的D2SE安装路径）  
2、将install下面的D2SEPatch.dll、msvcp140.dll、vcruntime140.dll复制到D:\Diablo II D2SE（具体目录要看你的D2SE安装路径）  
3、执行install下面的patch_D2SE.py对“D2SE启动游戏.exe”进行patch，让它调用我们的dll（方法参考大箱子PlugY）。当然，你也可以参考我的python程序自己手动patch，或者通过其他远程注入的方式（patch目录下面是我已经打好patch的exe，可以直接使用）  
4、跟原来一样双击“D2SE启动游戏.exe”就可以了  
5、你也可以像1.13c一样，将1.13d的标准dll全部放到1.13d的目录下  